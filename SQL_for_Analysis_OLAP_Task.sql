SET search_path TO sh, public;

-- task 1 
SELECT p.prod_category_desc, SUM(s.amount_sold) AS total_sales_amount
FROM sales s
JOIN products p ON s.prod_id = p.prod_id
WHERE s.time_id BETWEEN '1998-01-10' AND '2001-11-23'
GROUP BY p.prod_category_desc
ORDER BY total_sales_amount DESC;

-- task 2
SELECT co.country_region AS region_name, AVG(s.amount_sold) AS avg_sales_amount -- "quantity_sold" or "amount_sold" please give me a feedback after checking this task. :)
FROM sales s
JOIN customers c ON s.cust_id = c.cust_id
JOIN countries co ON c.country_id = co.country_id
WHERE s.prod_id = '113'
GROUP BY co.country_region;

-- task 3
SELECT c.cust_id, c.cust_first_name, c.cust_last_name, SUM(s.amount_sold) AS total_sales_amount
FROM sales s
JOIN customers c ON s.cust_id = c.cust_id
GROUP BY c.cust_id, c.cust_first_name, c.cust_last_name
ORDER BY total_sales_amount DESC
LIMIT 5;

